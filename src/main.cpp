/*
 * main.cpp
 *
 *  Created on: 26.11.2014
 *      Author: clemens
 */

#include <iostream>
#include <string>

#include "sha256.h"

using namespace std;


int main(int argc, char **argv) {
	string input = "";
	uint8_t hash[64];

	sha256( (uint8_t*)input.c_str(), input.length(), hash);

	cout<<hash<<" -"<<endl;

	return 0;
}
