//============================================================================
// Name        : sha256.cpp
// Author      : Clemens Reisner
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C, Ansi-style
//============================================================================

#include <iostream>
#include <cstring>
#include <cstdint>
#include <cstdio>

using namespace std;

#define LEADING_ONE  0x80
#define PADDING_ZERO 0x00

#define ROTR(n,x)	((uint32_t)((x>>n) | (x<<(32-n))))
#define ROTL(n,x)	((uint32_t)((x<<n) | (x>>(32-n))))
#define SHR(n,x)	((uint32_t)(x>>n))

#define CH(x,y,z)   ((uint32_t)((x & y) ^ ( (! x) & z)))
#define MAJ(x,y,z)  ((uint32_t)((x & y) ^ (x & z) ^ (y & z)))
#define BSIG0(x)  	((uint32_t)(ROTR(2,(x))  ^ ROTR(13,(x)) ^ ROTR(22,(x))))
#define BSIG1(x)  	((uint32_t)(ROTR(6,(x))  ^ ROTR(11,(x)) ^ ROTR(25,(x))))
#define SSIG0(x)  	((uint32_t)(ROTR(7,(x))  ^ ROTR(18,(x)) ^ SHR(3,(x))))
#define SSIG1(x)  	((uint32_t)(ROTR(17,(x)) ^ ROTR(19,(x)) ^ SHR(10,(x))))

const uint32_t K[64] = {  	0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5,
							0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
							0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3,
							0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
							0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc,
							0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
							0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7,
							0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
							0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,
							0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
							0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3,
							0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
							0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5,
							0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
							0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
							0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2  };

void sha256PrepareM(uint32_t* M, uint8_t* in, uint64_t len, uint64_t n)
{
	uint8_t* p = (uint8_t*) M;
	uint64_t i,j;

	// Prepare 64 byte of the message in W. Add padding if necessary.
	for(i = 0; i < 64; i++) {
		j = n*64 + i;
		if( j < len ) {
			p[i] = in[j];
		} else if( (j == len)) {
			p[i] = 0x80;
		} else if( i != 56) {
			p[i] = 0x00;
		} else if( i == 56  && (j > len) ){
			*((uint64_t*)(p+i)) = (n+1)*64;
			break;
		}
	}
}

void sha256PrepareW(uint32_t* W)
{
	unsigned i;

	// Prepare he rest of the work buffer
	for(i = 16; i < 64; i++) {
		W[i] = SSIG1(W[i-2]) + W[i-7] + SSIG0(i-15) + W[i-16];
	}
}



void sha256Trans(uint32_t* W, uint32_t* H)
{
	uint32_t a,b,c,d,e,f,g,h,T1,T2;

	a = H[0];
	b = H[1];
	c = H[2];
	d = H[3];
	e = H[4];
	f = H[5];
	g = H[6];
	h = H[7];

	for(unsigned t = 0; t < 64; t++) {
		T1 = h + BSIG1(e) + CH(e,f,g) + K[t] + W[t];
		T2 = BSIG0(a) + MAJ(a,b,c);
		h = g;
		g = f;
		f = e;
		e = d + T1;
		d = c;
		c = b;
		b = a;
		a = T1 + T2;
	}

	H[0] = a + H[0];
	H[1] = b + H[1];
	H[2] = c + H[2];
	H[3] = d + H[3];
	H[4] = e + H[4];
	H[5] = f + H[5];
	H[6] = g + H[6];
	H[7] = h + H[7];
}

void sha256Final(uint32_t* H, uint8_t* D) {
	uint8_t* p = (uint8_t *) H;
	for(unsigned i = 0; i < 32; i++) {
		sprintf((char*)(D+i*2),"%02x",p[i]);
	}
}


void sha256(uint8_t* in, size_t len, uint8_t* D)
{
    uint32_t W[64], H[8], N;

    // determine number of 64 byte message blocks
	N = (len / 64) + (((len % 64) < 9) ? 2 : 1);

	H[0] = 0x6a09e667;
	H[1] = 0xbb67ae85;
	H[2] = 0x3c6ef372;
	H[3] = 0xa54ff53a;
	H[4] = 0x510e527f;
	H[5] = 0x9b05688c;
	H[6] = 0x1f83d9ab;
	H[7] = 0x5be0cd19;

	for(uint64_t i = 0; i < N; i++) {
		sha256PrepareM(W, in, len, i);
		sha256PrepareW(W);
		sha256Trans(W,H);
	}
	sha256Final(H, D);


}


