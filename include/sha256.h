/*
 * sha256.h
 *
 *  Created on: 23.11.2014
 *      Author: clemens
 */

#ifndef SHA256_H_
#define SHA256_H_

#include <cstdint>

void sha256(uint8_t* in, size_t len, uint8_t* D);
void sha256PrepareM(uint32_t* M, uint8_t* in, uint64_t len, uint64_t n);
void sha256PrepareW(uint32_t* W);
void sha256Final(uint32_t* H, uint8_t* D);

#endif /* SHA256_H_ */
