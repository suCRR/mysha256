/*
 * test_sha256.cpp
 *
 *  Created on: 23.11.2014
 *      Author: clemens
 */

#include <cstdio>
#include <CppUTest/UtestMacros.h>
#include <CppUTest/CommandLineTestRunner.h>
#include "sha256.h"

using namespace std;


TEST_GROUP(sha256)
{
};

TEST(sha256, testEmptyInputString)
{
	char D[64];

	sha256((uint8_t*) "", 0, (uint8_t*) D);
	STRCMP_EQUAL("e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855",D);
}

TEST_GROUP(sha256Final)
{
};

TEST(sha256Final, testRandomString)
{
	uint8_t  D[65];
	uint32_t H[8] = { 0x03020100, 0x07060504, 0x0B0A0908, 0x0F0E0D0C, 0x13121110, 0x17161514, 0x1B1A1918, 0x1F1E1D1C };
	sha256Final(H, D);
	D[64] = 0;

	STRCMP_EQUAL("000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f", (char*) D);
}


TEST_GROUP(sha256PrepareM)
{
};


TEST(sha256PrepareM, testEmptyString)
{
	uint32_t M[16];

	sha256PrepareM(M, (uint8_t*)"", 0, 0);

	LONGS_EQUAL(0x00000080, M[0]);
	for(unsigned i = 1; i < 14; i++) {
		LONGS_EQUAL(0x00000000, M[i]);
	}
	LONGS_EQUAL(0x00000040, M[14]);
	LONGS_EQUAL(0x00000000, M[15]);
}

TEST(sha256PrepareM, testOneByteString)
{
	uint32_t M[16];

	sha256PrepareM(M, (uint8_t*)"A", 1, 0);

	LONGS_EQUAL(0x00008041, M[0]);
	for(unsigned i = 1; i < 14; i++) {
		LONGS_EQUAL(0x00000000, M[i]);
	}
	LONGS_EQUAL(0x00000040, M[14]);
	LONGS_EQUAL(0x00000000, M[15]);
}

TEST(sha256PrepareM, test55ByteString)
{
	uint32_t M[16];

	sha256PrepareM(M, (uint8_t*)"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 55, 0);

	for(unsigned i = 0; i < 13; i++) {
		LONGS_EQUAL(0x41414141, M[i]);
	}
	LONGS_EQUAL(0x80414141, M[13]);
	LONGS_EQUAL(0x00000040, M[14]);
	LONGS_EQUAL(0x00000000, M[15]);
}

TEST(sha256PrepareM, test56ByteString)
{
	uint32_t M[16];

	sha256PrepareM(M, (uint8_t*)"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 56, 0);

	for(unsigned i = 0; i < 14; i++) {
		LONGS_EQUAL(0x41414141, M[i]);
	}
	LONGS_EQUAL(0x00000080, M[14]);
	LONGS_EQUAL(0x00000000, M[15]);

	sha256PrepareM(M, (uint8_t*)"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 55, 1);

	for(unsigned i = 0; i < 14; i++) {
		LONGS_EQUAL(0x00000000, M[i]);
	}
	LONGS_EQUAL(0x00000080, M[14]);
	LONGS_EQUAL(0x00000000, M[15]);
}

TEST(sha256PrepareM, test65ByteString)
{
	uint32_t M[16];
	sha256PrepareM(M, (uint8_t*)"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 65, 0);

	for(unsigned i = 0; i < 16; i++) {
		LONGS_EQUAL(0x41414141, M[i]);
	}

	sha256PrepareM(M, (uint8_t*)"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 65, 1);

	LONGS_EQUAL(0x00008041, M[0]);
	for(unsigned i = 1; i < 14; i++) {
		LONGS_EQUAL(0x00000000, M[i]);
	}
	LONGS_EQUAL(0x00000080, M[14]);
	LONGS_EQUAL(0x00000000, M[15]);
}

int main(int ac, char** av)
{

	return RUN_ALL_TESTS(ac, av);
}
